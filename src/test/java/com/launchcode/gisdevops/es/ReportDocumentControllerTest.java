package com.launchcode.gisdevops.es;

import com.launchcode.gisdevops.LocationRepository;
import com.launchcode.gisdevops.models.Location;
import com.launchcode.gisdevops.models.Report;
import com.launchcode.gisdevops.util.WktHelper;
import com.launchcode.org.AbstractBaseRestIntegrationTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
public class ReportDocumentControllerTest extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Autowired
    private LocationRepository locationRepository;

    private static final Date YESTERDAY = new Date(System.currentTimeMillis()-24*60*60*1000);

    @Before
    public void setup(){
        reportDocumentRepository.deleteAll();
        locationRepository.deleteAll();
    }

    @After
    public void tearDown(){
        reportDocumentRepository.deleteAll();
        locationRepository.deleteAll();
    }

    @Test
    public void pathWorks() throws Exception {
        this.mockMvc.perform(get("/api/es/report/")).andExpect(status().isOk());
    }

    private static String parseDateString(Date date)  {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    private static ReportDocument reportDocument(String location, Date date) {
        return new ReportDocument(location, 22, "cases", "mycophenilia", "state", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"), date, parseDateString(YESTERDAY) );
    }

    private static Location createLocation(String country, String state) {
        return new Location(country, state, WktHelper.wktToGeometry("POLYGON((10 10, 20 10, 20 20, 10 20, 10 10),\n" +
                "(13 13, 17 13, 17 17, 13 17, 13 13))"));
    }

//TODO: why doesn't this work?
//    @Test
//    public void canGetFeaturesByDate() throws Exception {
//        Report report = new Report( new SimpleDateFormat("yyyy-MM-dd").parse("2002-05-01"), "State", "dog cases", 9, "cases", "XYZ", "NA", "NA", "Metropolis", "2002-05-01");
//        reportDocumentRepository.save(new ReportDocument(report));
//        this.mockMvc.perform(get("/api/es/report?date=2002-05-01"))
//        .andExpect(status().isOk())
//        .andExpect(jsonPath("$.type", equalTo("FeatureCollection")))
//        .andExpect(jsonPath("$", hasSize(1)));
//    }
}
