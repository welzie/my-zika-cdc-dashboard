package com.launchcode.org;

import com.launchcode.gisdevops.*;
import com.launchcode.gisdevops.dto.ReportDTO;
import com.launchcode.gisdevops.es.ReportDocument;
import com.launchcode.gisdevops.es.ReportDocumentRepository;
import com.launchcode.gisdevops.models.Location;
import com.launchcode.gisdevops.models.Report;
import com.launchcode.gisdevops.util.WktHelper;
import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.util.Lists.newArrayList;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class})
@TestPropertySource(locations = "classpath:application-test.properties")
@AutoConfigureMockMvc
public class ReportControllerTest extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Before
    public void setup(){
        reportRepository.deleteAll(); locationRepository.deleteAll(); reportDocumentRepository.deleteAll();
    }

    @After
    public void tearDown(){
        reportRepository.deleteAll(); locationRepository.deleteAll(); reportDocumentRepository.deleteAll();
    }

    private static final Date YESTERDAY = new Date(System.currentTimeMillis()-24*60*60*1000);

    @Test
    public void canPostReport() throws Exception {
        locationRepository.save(createLocation("Brazil", "Rondonia"));
        ReportDTO reportDTO = new ReportDTO(new Date(), "Brazil-Rondonia", "State", "dog cases", "BR0001", "NA", "NA", 22, "cases");
        // Test the resposnse and the json returned
        this.mockMvc.perform(post("/api/report")
                .content(json(reportDTO))
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.dataField", equalTo(reportDTO.getDataField())));

        // Test that data made it to the db
        // make sure report is in Postgres
        Report report = reportRepository.findAll().get(0);
        assertEquals(reportDTO.getDataField(), report.getDataField());
        assertEquals(reportDTO.getDataFieldCode(), report.getDataFieldCode());
        assertEquals(reportDTO.getLocation(), report.getLocation());
        assertEquals(reportDTO.getLocationType(), report.getLocationType());
        assertEquals(reportDTO.getReportDate(), report.getReportDate());
        assertEquals(reportDTO.getTimePeriod(), report.getTimePeriod());
        assertEquals(reportDTO.getTimePeriodType(), report.getTimePeriodType());
        assertEquals(reportDTO.getUnit(), report.getUnit());
        assertEquals(reportDTO.getValue(), report.getValue());
        // make sure report is in Elasticsearch
        ReportDocument reportDocument = reportDocumentRepository.findAll().iterator().next();
        assertEquals(reportDTO.getDataField(), reportDocument.getDataField());
        assertEquals(reportDTO.getLocation(), reportDocument.getLocation());
        assertEquals(reportDTO.getLocationType(), reportDocument.getLocationType());
        assertEquals(reportDTO.getReportDate(), reportDocument.getDate());
        assertEquals(reportDTO.getUnit(), reportDocument.getUnit());
        assertEquals(reportDTO.getValue(), reportDocument.getValue());
    }

    private static String parseDateString(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    private static Location createLocation(String country, String state) {
        return new Location(country, state, WktHelper.wktToGeometry("POLYGON((10 10, 20 10, 20 20, 10 20, 10 10),\n" +
                    "(13 13, 17 13, 17 17, 13 17, 13 13))"));
    }

}