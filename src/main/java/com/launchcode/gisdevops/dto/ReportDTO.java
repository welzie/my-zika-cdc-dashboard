package com.launchcode.gisdevops.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReportDTO {

    private Date reportDate;
    private String location;
    private String locationType;
    private String dataField;
    private String dataFieldCode;
    private String timePeriod;
    private String timePeriodType;
    private String unit;
    private Integer value;

    public ReportDTO() {
    }

    public ReportDTO(Date reportDate, String location, String locationType, String dataField, String dataFieldCode, String timePeriod, String timePeriodType, Integer value, String unit) {
        this.reportDate = reportDate;
        this.location = location;
        this.locationType = locationType;
        this.dataField = dataField;
        this.dataFieldCode = dataFieldCode;
        this.timePeriod = timePeriod;
        this.timePeriodType = timePeriodType;
        this.value = value;
        this.unit = unit;
    }

    public String getReportDateString() {
        if (reportDate != null) {
            return new SimpleDateFormat("yyyy-MM-dd").format(reportDate);
        } else {
            return "";
        }
    }

    public String getState() {
        if (this.getLocationType().toLowerCase() == "state"){
            return this.getLocation().split("-")[1];
        } else if (this.getLocationType().toLowerCase() == "country") {
            return "NA";
        }
        return "NA";
    }

    public String getCountry() {
        if (this.getLocationType().toLowerCase() == "country") {
            return this.getLocation();
        } else if (this.getLocationType().toLowerCase() == "state") {
            return this.getLocation().split("-")[0];
        }
        return "NA";
    }


    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTimePeriodType() {
        return timePeriodType;
    }

    public void setTimePeriodType(String timePeriodType) {
        this.timePeriodType = timePeriodType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
