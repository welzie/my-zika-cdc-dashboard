package com.launchcode.gisdevops.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class FeatureSerializer extends StdSerializer<Feature> {
    protected FeatureSerializer(Class<Feature> t) {
        super(t);
    }

    @Override
    public void serialize(Feature value, JsonGenerator gen, SerializerProvider provider) throws IOException {

    }
}
