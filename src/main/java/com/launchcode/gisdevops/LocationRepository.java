package com.launchcode.gisdevops;

import com.launchcode.gisdevops.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface LocationRepository extends JpaRepository<Location, Integer> {

    List<Location> findByCountryNormalizedAndStateNormalized(String country, String state);
}
