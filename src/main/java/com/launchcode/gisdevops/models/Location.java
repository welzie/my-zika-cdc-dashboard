package com.launchcode.gisdevops.models;

import com.launchcode.gisdevops.models.Report;
import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.*;
import java.util.List;

@Entity
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ID_0")
    private Long id_0;
    @Column(name = "ISO")
    private String iso;
    @Column(name = "NAME_0")
    private String countryName;
    @Column(name = "ID_1")
    private String id_1;
    @Column(name = "NAME_1")
    private String stateName;
    @Column(name = "HASC_1")
    private String hasc_1;
    @Column(name = "CCN_1")
    private String ccn_1;
    @Column(name = "CCA_1")
    private String cca_1;
    @Column(name = "TYPE_1")
    private String type_1;
    @Column(name = "ENGTYPE_1")
    private String engtype_1;
    @Column(name = "NL_NAME_1")
    private String nl_name_1;
    @Column(name = "VARNAME_1")
    private String varname_1;
    @Column(name = "name_0_normalized")
    private String countryNormalized;
    @Column(name = "name_1_normalized")
    private String stateNormalized;

    private Geometry geom;

    public Location() {
    }

    public Location(String name_0, String name_1, Geometry geom) {
        this.countryName = name_0;
        this.stateName = name_1;
        this.countryNormalized = name_0;
        this.stateNormalized = name_1;
        this.geom = geom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_0() {
        return id_0;
    }

    public void setId_0(Long id_0) {
        this.id_0 = id_0;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getId_1() {
        return id_1;
    }

    public void setId_1(String id_1) {
        this.id_1 = id_1;
    }

    public String getHasc_1() {
        return hasc_1;
    }

    public void setHasc_1(String hasc_1) {
        this.hasc_1 = hasc_1;
    }

    public String getCcn_1() {
        return ccn_1;
    }

    public void setCcn_1(String ccn_1) {
        this.ccn_1 = ccn_1;
    }

    public String getCca_1() {
        return cca_1;
    }

    public void setCca_1(String cca_1) {
        this.cca_1 = cca_1;
    }

    public String getType_1() {
        return type_1;
    }

    public void setType_1(String type_1) {
        this.type_1 = type_1;
    }

    public String getEngtype_1() {
        return engtype_1;
    }

    public void setEngtype_1(String engtype_1) {
        this.engtype_1 = engtype_1;
    }

    public String getNl_name_1() {
        return nl_name_1;
    }

    public void setNl_name_1(String nl_name_1) {
        this.nl_name_1 = nl_name_1;
    }

    public String getVarname_1() {
        return varname_1;
    }

    public void setVarname_1(String varname_1) {
        this.varname_1 = varname_1;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryNormalized() {
        return countryNormalized;
    }

    public void setCountryNormalized(String countryNormalized) {
        this.countryNormalized = countryNormalized;
    }

    public String getStateNormalized() {
        return stateNormalized;
    }

    public void setStateNormalized(String stateNormalized) {
        this.stateNormalized = stateNormalized;
    }
}
