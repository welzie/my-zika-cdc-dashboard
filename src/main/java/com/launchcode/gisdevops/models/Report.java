package com.launchcode.gisdevops.models;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Report {


    private static final long serialVersionUID = 1L;

    // report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,value,unit
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date reportDate;
    private String reportDateString;
    private String location;
    private String locationType;
    private String dataField;
    private String dataFieldCode;
    private String timePeriod;
    private String timePeriodType;
    private Integer value;
    private String unit;

    public Report() {
    }

    public Report(Date reportDate, String locationType, String dataField, Integer value, String unit, String dataFieldCode, String timePeriod, String timePeriodType, String location, String reportDateString) {
        this.reportDate = reportDate;
        this.location = location;
        this.locationType = locationType;
        this.dataField = dataField;
        this.value = value;
        this.unit = unit;
        this.dataFieldCode = dataFieldCode;
        this.timePeriod = timePeriod;
        this.timePeriodType = timePeriodType;
        this.reportDateString = reportDateString;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDataFieldCode() {
        return dataFieldCode;
    }

    public void setDataFieldCode(String dataFieldCode) {
        this.dataFieldCode = dataFieldCode;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public String getTimePeriodType() {
        return timePeriodType;
    }

    public void setTimePeriodType(String timePeriodType) {
        this.timePeriodType = timePeriodType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getReportDateString() {
        return reportDateString;
    }

    public void setReportDateString(String reportDateString) {
        this.reportDateString = reportDateString;
    }
}

