package com.launchcode.gisdevops;

import com.launchcode.gisdevops.dto.ReportDTO;
import com.launchcode.gisdevops.es.ReportDocument;
import com.launchcode.gisdevops.es.ReportDocumentRepository;
import com.launchcode.gisdevops.models.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Date;

@Controller
@RequestMapping(value = "/api/report")
public class ReportController {


    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;


    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Report> saveNewReport(@RequestBody ReportDTO reportDTO) {
        Report report = new Report(reportDTO.getReportDate(), reportDTO.getLocationType(), reportDTO.getDataField(), reportDTO.getValue(), reportDTO.getUnit(),
                reportDTO.getDataFieldCode(), reportDTO.getTimePeriod(), reportDTO.getTimePeriodType(), reportDTO.getLocation(), reportDTO.getReportDateString());
        reportRepository.save(report);
        ReportDocument reportDocument = new ReportDocument(report);
        reportDocumentRepository.save(reportDocument);
        return new ResponseEntity<>(report, HttpStatus.CREATED);
    }

}
