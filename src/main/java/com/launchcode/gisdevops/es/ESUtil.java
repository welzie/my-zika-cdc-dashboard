package com.launchcode.gisdevops.es;

import com.launchcode.gisdevops.ReportRepository;
import com.launchcode.gisdevops.models.Report;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ESUtil {

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Autowired
    private ReportRepository reportRepository;

    private static Logger logger = LoggerFactory.getLogger(ESUtil.class);

    public void reindex() {
        logger.info("Deleting all ElasticSearch Indices");
        reportDocumentRepository.deleteAll();
        List<ReportDocument> reportDocuments = new ArrayList<>();
        for(Report report : reportRepository.findAll()) {
            report.setReportDateString(new SimpleDateFormat("yyyy-MM-dd").format(report.getReportDate()));
            reportDocuments.add(new ReportDocument(report));
        }
        reportDocumentRepository.save(reportDocuments);
    }

    public List<ReportDocument> totalDocs() {
        List<ReportDocument> reportDocuments = new ArrayList<>();
        Iterator<ReportDocument> iterator = reportDocumentRepository.findAll().iterator();
        while(iterator.hasNext()) {
            ReportDocument reportDocument = iterator.next();
            reportDocuments.add(reportDocument);
        }
        return reportDocuments;
    }

}
