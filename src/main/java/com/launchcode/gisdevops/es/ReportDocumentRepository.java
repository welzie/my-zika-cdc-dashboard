package com.launchcode.gisdevops.es;

import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Iterator;

@Repository
public interface ReportDocumentRepository extends ElasticsearchRepository<ReportDocument, Integer> {

    /* You'll may want to change this to make your date search more precise*/
    public Iterable<ReportDocument> findByStringDate(String stringDate);



}