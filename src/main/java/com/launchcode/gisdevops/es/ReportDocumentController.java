package com.launchcode.gisdevops.es;

import com.launchcode.gisdevops.LocationRepository;
import com.launchcode.gisdevops.models.Location;
import com.launchcode.gisdevops.util.Feature;
import com.launchcode.gisdevops.util.FeatureCollection;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Controller
@RequestMapping(value = "/api/es/report")
public class ReportDocumentController {

    private static Logger logger = LoggerFactory.getLogger(ESConfiguration.class);

    @Autowired
    private ReportDocumentRepository reportDocumentRepository;

    @Autowired
    private LocationRepository locationRepository;

    @ApiOperation("Return all Features in Elasticsearch for a certain date or all features if no date given.")
    @GetMapping
    public ResponseEntity<FeatureCollection> getFeatures(@RequestParam(name = "date") @ApiParam("Report date YYYY-MM-DD") Optional<String> dateParam) {
        /* You may have to write a few tests to get this to filter by date */
        FeatureCollection featureCollection = new FeatureCollection();
        Iterator<ReportDocument> iterator;
        if (dateParam.isPresent()) {
            iterator = reportDocumentRepository.findByStringDate(dateParam.get()).iterator();
        } else {
            iterator = reportDocumentRepository.findAll().iterator();
        }
        if(!iterator.hasNext()) {
            return new ResponseEntity<>(featureCollection, HttpStatus.OK);
        }
        while(iterator.hasNext()) {
            ReportDocument reportDocument = iterator.next();
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("dateString", reportDocument.getStringDate());
            properties.put("value", reportDocument.getValue());
            List<Location> locations = locationRepository.findByCountryNormalizedAndStateNormalized(reportDocument.getCountry(), reportDocument.getState());
            if(locations != null && !locations.isEmpty()) {
                Feature feature = new Feature(locations.get(0).getGeom(), properties);
                featureCollection.addFeature(feature);
            }
        }
        return new ResponseEntity<>(featureCollection, HttpStatus.OK);
    }

}
