package com.launchcode.gisdevops.es;

import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.NodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Configuration
public class ESConfiguration {
    @Value("${elasticsearch.home:/usr/local/Cellar/elasticsearch/6.1.3/}")
    private String elasticsearchHome;

    private static Logger logger = LoggerFactory.getLogger(ESConfiguration.class);

    @Bean
    public NodeBuilder nodeBuilder() {
        return new NodeBuilder();
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        final Path tmpDir;
        try {
            tmpDir = Files.createTempDirectory(Paths.get(System.getProperty("java.io.tmpdir")), "elasticsearch_data");
        } catch (IOException e) {
            logger.error("Cannot create temp dir", e);
            throw new RuntimeException();
        }
        logger.debug(tmpDir.toAbsolutePath().toString());

        Settings.Builder elasticsearchSettings = Settings.settingsBuilder()
                .put("http.enabled", "false")
                .put("path.data", tmpDir.toAbsolutePath().toString())
                .put("path.home", elasticsearchHome);

        return new ElasticsearchTemplate(nodeBuilder()
                .local(true)
                .settings(elasticsearchSettings.build())
                .node()
                .client());


    }

}
