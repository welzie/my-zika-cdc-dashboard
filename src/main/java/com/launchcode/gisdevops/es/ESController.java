package com.launchcode.gisdevops.es;

import com.launchcode.gisdevops.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

@Controller
@RequestMapping(value = "/api/_cluster")
public class ESController {

    @Autowired
    private ESUtil esUtil;

    @Autowired
    private ReportRepository reportRepository;

    /**
     *  TODO: Run this everytime you start your server.
     *
     * @return
     */
    @RequestMapping(value = "/reindex", method = RequestMethod.POST)
    public ResponseEntity<String> reindex() {
       esUtil.reindex();
        return new ResponseEntity<>("Health", HttpStatus.OK);
    }

    @RequestMapping(value = "/{index}/docs", method = RequestMethod.GET)
    public ResponseEntity<List<ReportDocument>> totalDocs(@PathVariable String index) {
        return new ResponseEntity<List<ReportDocument>>(esUtil.totalDocs(), HttpStatus.OK);
    }

}
