package com.launchcode.gisdevops.es;

import com.launchcode.gisdevops.models.Report;
import com.vividsolutions.jts.geom.Geometry;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.geo.Point;
import org.springframework.data.geo.Polygon;

import java.util.Date;

import static com.google.common.collect.Lists.newArrayList;

@Document(indexName = "report", type="stateReport")
public class ReportDocument {

    @Id
    private Long id;
    private String location;
    private Integer value;
    private String unit;
    private String dataField;
    private String locationType;
    private Date date;
    private String stringDate;

    public ReportDocument() {
    }

    public ReportDocument(String location, Integer value, String unit, String dataField, String locationType, Geometry geometry, Date date, String stringDate) {
        this.location = location;
        this.value = value;
        this.unit = unit;
        this.dataField = dataField;
        this.locationType = locationType;
        this.date = date;
        this.stringDate = stringDate;
    }

    public ReportDocument(Report report) {
        this.id = report.getId();
        this.value = report.getValue();
        this.unit = report.getUnit();
        this.dataField = report.getDataField();
        this.locationType = report.getLocationType();
        this.location = report.getLocation();
        this.date = report.getReportDate();
        this.stringDate = report.getReportDateString();
    }

    public String getState() {
        String[] split = this.getLocation().split("-");
        if (split != null && split.length > 1) {
            return split[1];
        }
        return "";
    }

    public String getCountry() {
        String[] split = this.getLocation().split("-");
        if (split != null && split.length > 0) {
            return split[0];
        }
        return "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDataField() {
        return dataField;
    }

    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStringDate() {
        return stringDate;
    }

    public void setStringDate(String stringDate) {
        this.stringDate = stringDate;
    }
}
