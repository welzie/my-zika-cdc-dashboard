/* global $, document, ol */
$(document).ready(() => {
  const view = new ol.View({
    center: [-9101767, 1823912],
    zoom: 5,
  });

  const map = new ol.Map({
    controls: ol.control.defaults().extend([
      new ol.control.FullScreen({
        source: 'fullscreen',
      }),
    ]),
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM(),
      }),
    ],
    target: 'map',
    view,
  });

  const getColor = (feature) => {
    let result = null;
    if (feature.get('value') >= 100) {
      result = 'rgba(255, 0, 0, 0.7)';
    } else if (feature.get('value') >= 50) {
      result = 'rgba(255,165,0, 0.5)';
    } else if (feature.get('value') >= 5) {
      result = 'rgba(255,255,102, 0.5)';
    } else {
      result = 'rgba(154,205,50, 0.5)';
    }
    return result;
  };

  const stateLayer = new ol.layer.Vector({
    source: new ol.source.Vector({
      url: 'http://localhost:8080/api/es/report/?date=2016-03-05',
      format: new ol.format.GeoJSON(),
    }),
    style: (feature) => {
      return new ol.style.Style({
        fill: new ol.style.Fill({
          color: getColor(feature),
        }),
        stroke: new ol.style.Stroke({
          color: '#319FD3',
          width: 1,
        }),
      });
    },
  });

  map.addLayer(stateLayer);

  map.on('click', (event) => {
    map.forEachFeatureAtPixel(event.pixel, (feature) => {
      const location = feature.get('NAME_1');
      $('#sidepanel').empty();
      $.getJSON('/report/?location=' + location, {})
        .done((json) => {
          if (json.features) {
            const reportTexts = [];
            json.features.forEach((f) => {
              const dataField = f.properties.dataField;
              const valueReported = f.properties.valueReported;
              const unit = f.properties.unit;
              reportTexts.push(`<li>${dataField} - ${valueReported} ${unit}</li>`);
            });
            $('#sidepanel').append(`
                            <div class="sidepanel-text">
                                <h1>${feature.properties.location}</h1>
                                <div class="sidepanel-reports">
                                    <ul>${reportTexts.join('')}</ul>
                                </div>
                            </div>
                        `);
          }
        });
    });
  });

});
