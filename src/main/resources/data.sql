BEGIN;
COPY location (ID_0,ISO,NAME_0,ID_1,NAME_1,HASC_1,CCN_1,CCA_1,TYPE_1,ENGTYPE_1,NL_NAME_1,VARNAME_1,geom) FROM '/Users/welzie/projects/practice/zika-cdc-dashboard/locations.csv';
COPY report(report_date, location, location_type, data_field, data_field_code, time_period, time_period_type, value, unit) FROM '/Users/welzie/projects/practice/zika-cdc-dashboard/all_reports.csv' DELIMITER ',' CSV HEADER;

UPDATE location SET name_0_normalized = unaccent(name_0);
UPDATE location SET name_1_normalized = unaccent(name_1);

COMMIT;
